package com.devcamp.s50.task5430.restapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/devcamp-welcome")
public class CRandomNumber {
  @CrossOrigin
  @GetMapping("/random-double")
  public String randomDoubleNumber() {
    // tạo số ngẫu nhiên từ 1 đến 100
     double randomDoubleNumber = Math.random() * 99 + 1 ;
    return "Random value in double from 1 to 100: " + randomDoubleNumber;
  }

  @GetMapping("/random-int")
  public String randomIntNumber() {
    int randomIntNumber = 1 + (int) (Math.random() * (10 - 1));
    return "Random value in int from 1 to 10: " + randomIntNumber;
  }
}
